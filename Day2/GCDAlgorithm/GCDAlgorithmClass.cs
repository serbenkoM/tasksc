﻿using System;
using System.Diagnostics;

namespace EuclideanAlgorithm
{
    public static class GCDAlgorithmClass
    {
        public static int GetGCDEuclideanAlgorithm(int a, int b, ref long time)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            if (a == 0 || b == 0)
                throw new Exception();
            while (a != b)
            {
                if (a > b)
                    a = a - b;
                else
                    b = b - a;
            }
            watch.Stop();
            time += watch.ElapsedMilliseconds;
            return a;
        }

        public static int GetGCDEuclideanAlgorithm(int a, int b, int c, ref long time)
        {
            return GetGCDEuclideanAlgorithm(GetGCDEuclideanAlgorithm(a, b,ref time), c,ref time);
        }

        public static int GetGCDEuclideanAlgorithm(ref long time, params int[] array)
        {
            int gcd;
            for(int i = 0; i < array.Length; i++)
            {
                if(array[i] == 0)
                    throw new Exception();
            }
            gcd = GetGCDEuclideanAlgorithm(array[0], array[1], ref time);
            for (int i = 2; i < array.Length; i++)
            {
                gcd = GetGCDEuclideanAlgorithm(gcd, array[i], ref time);
            }
            return gcd;
        }

        /* public static int GetGCDEuclideanAlgorithm(out long time, params int[] array)
         {
             Stopwatch watch = new Stopwatch();
             watch.Start();
             int a, b;
             for (int i = 0; i < array.Length; i++)
             {
                 if (array[i] == 0)
                     throw new Exception();
             }
             a = array[0];
             for (int i = 1; i < array.Length; i++)
             {
                 b = array[i];
                 while (a != b)
                 {
                     if (a > b)
                         a = a - b;
                     else
                         b = b - a;
                 }
                 if (a == 1)
                     break;
             }
             watch.Stop();
             time = watch.ElapsedMilliseconds;
             return a;
         }*/
        public static int GetGCDBinaryAlgorithm(int a, int b, ref long time)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            if (a == 0)
            {
                watch.Stop();
                time += watch.ElapsedMilliseconds;
                return b;
            }
            if (b == 0)
            {
                watch.Stop();
                time += watch.ElapsedMilliseconds;
                return a;
            }
            if (a == b)
            {
                watch.Stop();
                time += watch.ElapsedMilliseconds;
                return a;
            }
            if ((a % 2 == 0) && (b % 2 == 0))
            {
                watch.Stop();
                time += watch.ElapsedMilliseconds;
                return 2 * GetGCDBinaryAlgorithm(a / 2, b / 2, ref time);
            }
            if ((a % 2 == 0) && (b % 2 != 0))
            {
                watch.Stop();
                time += watch.ElapsedMilliseconds;
                return GetGCDBinaryAlgorithm(a / 2, b, ref time);
            }

            if ((a % 2 != 0) && (b % 2 == 0))
            {
                watch.Stop();
                time += watch.ElapsedMilliseconds;
                return GetGCDBinaryAlgorithm(a, b / 2, ref time);
            }
            watch.Stop();
            time += watch.ElapsedMilliseconds;
            return GetGCDBinaryAlgorithm(b, Math.Abs(a - b), ref time);
        }

        public static int GetGCDBinaryAlgorithm(int a, int b, int c, ref long time)
        {
            return GetGCDBinaryAlgorithm(GetGCDBinaryAlgorithm(a, b, ref time), c, ref time);
        }

        public static int GetGCDBinaryAlgorithm(ref long time, params int[] array)
        {
            int a = array[0];
            for (int i = 1; i < array.Length; i++)
            {
                a = GetGCDBinaryAlgorithm(a, array[i], ref time);
            }
            return a;
        }
    }
}
