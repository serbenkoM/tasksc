﻿using System;
using System.Collections;
using NUnit.Framework;
using GCD;

namespace GCDAlgorithmTest
{
    [TestFixture]
    public class GCDAlgorithmClassTest
    {
        [Test]
        [TestCaseSource(typeof(DataClassEuclideanAlgorithmTwoArgs), "PositiveData")]
        [TestCaseSource(typeof(DataClassEuclideanAlgorithmTwoArgs), "ExeptionData")]
        public int GetGCDEuclideanAlgorithmTest(int a, int b, long time)
        {
            return GCDClass.GetGCDEuclideanAlgorithm(a, b, ref time);
        }

        [Test]
        [TestCaseSource(typeof(DataClassEuclideanAlgorithmThreeArgs), "PositiveData")]
        [TestCaseSource(typeof(DataClassEuclideanAlgorithmThreeArgs), "ExeptionData")]
        public int GetGCDEuclideanAlgorithmTest(int a, int b, int c, long time)
        {
            return GCDClass.GetGCDEuclideanAlgorithm(a, b, c, ref time);
        }

        [Test]
        [TestCaseSource(typeof(DataClassEuclideanAlgorithm), "PositiveData")]
        [TestCaseSource(typeof(DataClassEuclideanAlgorithm), "ExeptionData")]
        public int GetGCDEuclideanAlgorithmTest(long time, params int[] array)
        {
            return GCDClass.GetGCDEuclideanAlgorithm(ref time, array);
        }

        [Test]
        [TestCase(25,45,(long) 0, Result = 5)]
        public int GetGCDBinaryAlgorithmTest(int a, int b, ref long time)
        {
            return GCDClass.GetGCDBinaryAlgorithm(a, b, ref time);
        }

        #region DataClased
        public class DataClassEuclideanAlgorithmTwoArgs
        {
            public static IEnumerable PositiveData
            {
                get
                {
                    yield return new TestCaseData(18, 9, 0).Returns(9);
                    yield return new TestCaseData(20, 47, 0).Returns(1);
                }
            }
            public static IEnumerable ExeptionData
            {
                get
                {
                    yield return new TestCaseData(0, 15, 0).Throws(typeof(Exception));
                    yield return new TestCaseData(250, 0, 0).Throws(typeof(Exception));
                    yield return new TestCaseData(18, -9, 0).Throws(typeof(Exception));
                    yield return new TestCaseData(-45, 39, 0).Throws(typeof(Exception));
                }
            }
        }

        public class DataClassEuclideanAlgorithmThreeArgs
        {
            public static IEnumerable PositiveData
            {
                get
                {
                    yield return new TestCaseData(18, 9, 27, 0).Returns(9);
                    yield return new TestCaseData(20, 47, 143, 0).Returns(1);
                }
            }
            public static IEnumerable ExeptionData
            {
                get
                {
                    yield return new TestCaseData(0, 0, 0, 0).Throws(typeof(Exception));
                    yield return new TestCaseData(0, 0, 180, 0).Throws(typeof(Exception));
                    yield return new TestCaseData(0, 125, 180, 0).Throws(typeof(Exception));
                    yield return new TestCaseData(18, -9, 27, 0).Throws(typeof(Exception));
                    yield return new TestCaseData(45, 39, -2820, 0).Throws(typeof(Exception));
                    yield return new TestCaseData(-20, 47, 143, 0).Throws(typeof(Exception));
                }
            }
        }

        public class DataClassEuclideanAlgorithm
        {
            public static IEnumerable PositiveData
            {
                get
                {
                    yield return new TestCaseData(0, new int[] { 15, 9, 18, 102, 126 }).Returns(3);
                    yield return new TestCaseData(0, new int[] { 100, 25, 150, 250, 100, 2500 }).Returns(25);
                }
            }
            public static IEnumerable ExeptionData
            {
                get
                {
                    yield return new TestCaseData(0, new int[] { 100, 25, 0, 250, 100 }).Throws(typeof(Exception));
                    yield return new TestCaseData(0, new int[] { 0, 25, 150, 250, 100 }).Throws(typeof(Exception));
                    yield return new TestCaseData(0, new int[] {- 15, 9, 18, -102, 126 }).Throws(typeof(Exception));
                    yield return new TestCaseData(0, new int[] { 115, -11, 143, 18 }).Throws(typeof(Exception));
                    yield return new TestCaseData(0, new int[] { 100, 25, 150, 250, -100 }).Throws(typeof(Exception));
                }
            }
        }
        #endregion
    }
}
