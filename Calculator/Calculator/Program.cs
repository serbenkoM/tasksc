﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                try {
                        Console.WriteLine("Enter first number:");
                        int a = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter second number:");
                        int b = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter operation (+, -, *, /)");
                        String operation = Console.ReadLine();
                        switch (operation)
                        {
                            case "+":
                                Calculator.addition(a, b);
                                break;
                            case "-":
                                Calculator.subtraction(a, b);
                                break;
                            case "*":
                                Calculator.multiplication(a, b);
                                break;
                            case "/":
                                Calculator.division(a, b);
                                break;
                        default:
                            Console.WriteLine("Unknown operation.");
                            break;
                        }
                    }catch (System.FormatException)
                    {
                        Console.WriteLine("Error!  Require to enter a number.");
                    }
                Console.WriteLine("Enter 'q' to quit or 'Enter' to continue.");
                if (Console.ReadLine().Equals("q"))
                {
                    break;
                }
            }
        }
    }
}
