﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringRevert
{
    public class Revert
    {
        public List<String> revertStringOfList(List<string> list)
        {
           for(int i=0; i<list.Capacity; i++)
            {
                char[] resertString = list[i].ToCharArray();
                Array.Reverse(resertString);
                list.Insert(i,new string (resertString));
            }
            return list;
        }
    }
}
