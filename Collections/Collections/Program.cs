﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;

namespace Collections
{
    class Program
    {
        static Dictionary<int, string> dictionary = new Dictionary<int, string>();
        static Hashtable hashTable = new Hashtable();
        static void Main(string[] args)
        {
            string str = "qqwweerrttyyuuiioopp";
            Random random = new Random();
            int quantiyElements = random.Next(10000,1000000);
            Stopwatch sWatch = new Stopwatch();
            Console.WriteLine("Add elements in dictionaries.");
            Console.WriteLine("Hashtable: " + timeForAddElementsToHashtable(quantiyElements, str) + " ticks.");
            Console.WriteLine("Dictionary: " + timeForAddElementsToDictionary(quantiyElements, str) + " ticks.");
            int keyDelete = random.Next(10000, 1000000);
            Console.WriteLine("\nDelete element in dictionary.");
            Console.WriteLine("Hashtable: " + timeForDeleteElementFromHashtable(keyDelete) + " ticks.");
            Console.WriteLine("Dictionary: " + timeForDeleteElementFromDictionary(keyDelete) + " ticks.");
            int keySearch = random.Next(10000, 1000000);
            Console.WriteLine("\nSearch element in dictionary.");
            Console.WriteLine("Hashtable: " + timeForSearchElementInHashtable(keySearch) + " ticks.");
            Console.WriteLine("Dictionary: " + timeForSearchElementInDictionary(keySearch) + " ticks.");
            Console.Read();
        }

        public static long timeForAddElementsToHashtable(long quantityElements, string value)
        {
            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();
            for (int i = 0; i < quantityElements; i++)
            {
                hashTable.Add(i, value);
            }
            sWatch.Stop();
            return sWatch.ElapsedTicks;
        }

        public static long timeForAddElementsToDictionary(long quantityElements, string value)
        {
            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();
            for (int i = 0; i < quantityElements; i++)
            {
                dictionary.Add(i, value);
            }
            sWatch.Stop();
            return sWatch.ElapsedTicks;
        }

        public static long timeForDeleteElementFromDictionary(int key)
        {
            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();
            dictionary.Remove(key);
            sWatch.Stop();
            return sWatch.ElapsedTicks;
        }

        public static long timeForDeleteElementFromHashtable(int key)
        {
            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();
            hashTable.Remove(key);
            sWatch.Stop();
            return sWatch.ElapsedTicks;
        }

        public static long timeForSearchElementInDictionary(int key)
        {
            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();
            dictionary.ContainsKey(key);
            sWatch.Stop();
            return sWatch.ElapsedTicks;
        }

        public static long timeForSearchElementInHashtable(int key)
        {
            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();
            hashTable.ContainsKey(key);
            sWatch.Stop();
            return sWatch.ElapsedTicks;
        }
    }
}
