﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chief.Enums;
using Chief.Salads;
using Chief.VegetableFactory;
using Chief.Vegetables;

namespace Chief.Runner
{
    class MainClass
    {
        static void Main(string[] args)
        {
             
            Salad salad = new Salad();
            bool cycle = true;
            List<Vegetable> vegetablesForSalad = new List<Vegetable>();
            while (cycle)
            {
                Console.WriteLine("1. Veiw salads in txt file.\n2. Veiw salads in binary file.\n3. Veiw salads in xml file\n4. Create new salad.\n5. Exit");
                try
                {

                    int choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            foreach (Salad s in Utils.FileFunctions.ReadTxtFile())
                            {
                                Console.WriteLine("\nSalad");
                                foreach (Vegetable v in s.Vegetables)
                                    Console.WriteLine("\t" + v.GetType().Name + ". " + v.ToString());
                            }
                            break;
                        case 2:
                            foreach (Salad s in Utils.FileFunctions.ReadBinaryFile())
                            {
                                Console.WriteLine("\nSalad");
                                foreach (Vegetable v in s.Vegetables)
                                    Console.WriteLine("\t" + v.GetType().Name + ". " + v.ToString());
                            }
                            break;
                        case 3:                            
                            foreach(Vegetable v in Utils.FileFunctions.DeserializeFromXML().Vegetables)
                                    Console.WriteLine("\t" + v.GetType().Name + ". " + v.ToString());
                            break;
                        case 4:
                            salad = createSalad();
                            Console.WriteLine("Add salad in:\n\t1)binary file;\n\t2)txt file\n\t3)both files\n\t4)xml file");
                            int fileType = Convert.ToInt32(Console.ReadLine());
                            switch (fileType)
                            {
                                case 1:
                                    Utils.FileFunctions.AddSaladInBinaryFile(salad);
                                    break;
                                case 2:
                                    Utils.FileFunctions.AddSaladInTxtFile(salad);
                                    break;
                                case 3:
                                    Utils.FileFunctions.AddSaladInBinaryFile(salad);
                                    Utils.FileFunctions.AddSaladInTxtFile(salad);
                                    break;
                                case 4:
                                    Utils.FileFunctions.SerializeToXML(salad);
                                    break;
                                default:
                                    Console.WriteLine("Choose a menu item.");
                                    break;
                            }
                            saladInformation(salad);
                            break;
                        case 5:
                            cycle = false;
                            break;
                        default:
                            Console.WriteLine("Choose a menu item.");
                            break;
                    }
                }
                catch (System.FormatException)
                {
                    Console.WriteLine("Error!  Require to enter a number.");
                }
                catch (System.IO.FileNotFoundException)
                {
                    Console.WriteLine("File not found.");
                }
            }
        }

        public static void Input(out string country, out double weight, out double calories)
        {
            Console.WriteLine("Enter the origin country of vegetable: ");
            country = Console.ReadLine();
            Console.WriteLine("Enter calories of vegetable per 100 gramms: ");
            calories = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter weight of vegetable: ");
            weight = Convert.ToDouble(Console.ReadLine());
            if (calories <= 0 || weight <= 0)
                throw new Exceptions.NegativeValueException("Caloric value & weight shoudl be more then 0.");
        }

        public static Salad createSalad()
        {
            Salad salad = new Salad();
            bool cycleVegetables = true;
            string country;
            double weight, calories;
            while (cycleVegetables)
            {
                Console.WriteLine("Choose vegetables for salad");
                int i = 1;
                foreach (var value in Enum.GetValues(typeof(Enums.Vegetables)))
                {
                    Console.WriteLine(i++ + ". " + (Enums.Vegetables)value);
                }
                Console.WriteLine(i + ". Finish adding vegetables.");
                try
                {
                    int choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            Input(out country, out weight, out calories);
                            Avocado avocado = (Avocado)VegetableFactory.VegetableFactory.getVegetable("Avocado", country, weight, calories);
                            //vegetablesForSalad.Add(avocado);
                            salad.addVegetable(avocado);
                            break;
                        case 2:
                            Input(out country, out weight, out calories);
                            Cabbage cabbage = (Cabbage)VegetableFactory.VegetableFactory.getVegetable("Cabbage", country, weight, calories);
                            Console.WriteLine("Choose sort of cabbage");
                            i = 1;
                            foreach (var value in Enum.GetValues(typeof(Enums.CabbageSort)))
                            {
                                Console.WriteLine(i++ + ". " + (Enums.CabbageSort)value);
                            }
                            i = Convert.ToInt32(Console.ReadLine());
                            cabbage.Sort = ((Enums.CabbageSort)Enum.GetValues(typeof(Enums.CabbageSort)).GetValue(i - 1));
                            //vegetablesForSalad.Add(cabbage);
                            salad.addVegetable(cabbage);
                            break;
                        case 3:
                            Input(out country, out weight, out calories);
                            Celery celery = (Celery)VegetableFactory.VegetableFactory.getVegetable("Celery", country, weight, calories);
                            Console.WriteLine("Choose part of celery");
                            i = 1;
                            foreach (var value in Enum.GetValues(typeof(Enums.CeleryPart)))
                            {
                                Console.WriteLine(i++ + ". " + (Enums.CeleryPart)value);
                            }
                            i = Convert.ToInt32(Console.ReadLine());
                            celery.Part = ((Enums.CeleryPart)Enum.GetValues(typeof(Enums.CeleryPart)).GetValue(i - 1));
                            //vegetablesForSalad.Add(celery);
                            salad.addVegetable(celery);
                            break;
                        case 4:
                            Input(out country, out weight, out calories);
                            Cucumber cucumber = (Cucumber)VegetableFactory.VegetableFactory.getVegetable("Cucumber", country, weight, calories);
                            Console.WriteLine("Choose type of cucumber");
                            i = 1;
                            foreach (var value in Enum.GetValues(typeof(Enums.CucumberType)))
                            {
                                Console.WriteLine(i++ + ". " + (Enums.CucumberType)value);
                            }
                            i = Convert.ToInt32(Console.ReadLine());
                            cucumber.Type = ((Enums.CucumberType)Enum.GetValues(typeof(Enums.CucumberType)).GetValue(i - 1));
                            //vegetablesForSalad.Add(cucumber);
                            salad.addVegetable(cucumber);
                            break;
                        case 5:
                            Input(out country, out weight, out calories);
                            Fennel fennel = (Fennel)VegetableFactory.VegetableFactory.getVegetable("Fennel", country, weight, calories);
                            Console.WriteLine("Choose condition of fennel");
                            i = 1;
                            foreach (var value in Enum.GetValues(typeof(Enums.FennelCondition)))
                            {
                                Console.WriteLine(i++ + ". " + (Enums.FennelCondition)value);
                            }
                            i = Convert.ToInt32(Console.ReadLine());
                            fennel.Condition = ((Enums.FennelCondition)Enum.GetValues(typeof(Enums.FennelCondition)).GetValue(i - 1));
                            //vegetablesForSalad.Add(fennel);
                            salad.addVegetable(fennel);
                            break;
                        case 6:
                            Input(out country, out weight, out calories);
                            Tomato tomato = (Tomato)VegetableFactory.VegetableFactory.getVegetable("Tomato", country, weight, calories);
                            Console.WriteLine("Choose color of tomato");
                            i = 1;
                            foreach (var value in Enum.GetValues(typeof(Enums.TomatoColor)))
                            {
                                Console.WriteLine(i++ + ". " + (Enums.TomatoColor)value);
                            }
                            i = Convert.ToInt32(Console.ReadLine());
                            tomato.Color = ((Enums.TomatoColor)Enum.GetValues(typeof(Enums.TomatoColor)).GetValue(i - 1));
                            //vegetablesForSalad.Add(tomato);
                            salad.addVegetable(tomato);
                            break;
                        case 7:
                            cycleVegetables = false;
                            break;
                        default:
                            Console.WriteLine("Choose a menu item.");
                            break;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Error! No such type of the vegetable.");
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Error! No such vegetable.");
                }
                catch (Exceptions.NegativeValueException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (System.FormatException)
                {
                    Console.WriteLine("Error!  Require to enter a number.");
                }
                catch (Exceptions.VegetableAddingException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return salad;
        }

        public static void saladInformation(Salad salad)
        {
            try
            {
                Console.WriteLine("Total weight of salad is " + salad.getSaladWeight() + " gramms.");
                Console.WriteLine("Total calories in salad is " + salad.getSaladCalories() + ".");
                Console.WriteLine("\nVegetables is sorted by total caloric value.");
                foreach (Vegetable vegetable in salad.sortVegetablesByCalories())
                {
                    Console.WriteLine(vegetable.GetType().Name + ". " + "Total calories: "+vegetable.getTotalCalories());
                }
                Console.WriteLine("\nVegetables is sorted by used weight.");
                foreach (Vegetable vegetable in salad.sortVegetablesByWeight())
                {
                    Console.WriteLine(vegetable.GetType().Name + ". " + "Used weight: " + vegetable.usedWeight());
                }
                Console.WriteLine("\nEnter minimum of caloric value: ");
                double minCalories = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("\nEnter maximum of caloric value: ");
                double maxCalories = Convert.ToDouble(Console.ReadLine());
                List<Vegetable> vegetableList = new List<Vegetable>();
                vegetableList = salad.searchVegetables(minCalories, maxCalories);
                if(vegetableList.Count == 0)
                    Console.WriteLine("There are no vegetables in salad with caloric value betwen entered values");
                else
                    foreach (Vegetable vegetable in salad.searchVegetables(minCalories, maxCalories))
                {
                    Console.WriteLine(vegetable.GetType().Name);
                }
            }
            catch (System.FormatException)
            {
                Console.WriteLine("Error!  Require to enter a number.");
            }
        }
    }
}
