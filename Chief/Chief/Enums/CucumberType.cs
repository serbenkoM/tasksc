﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chief.Enums
{
    // Enumeration of types of cucumbers
    public enum CucumberType
    {
        pickled, fresh
    }
}
