﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chief.Enums
{
    public enum Vegetables
    {
        Avocado, Cabbage, Celery, Cucumber, Fennel, Tomato
    }
}
