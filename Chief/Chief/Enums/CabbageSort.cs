﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chief.Enums
{
    // Enumeration of sorts of cabbage
    public enum CabbageSort
    {
        white, kohlrabi, cauliflower, broccoli, beijing, brussels
    }
}
