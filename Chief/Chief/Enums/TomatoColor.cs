﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chief.Enums
{
    // Enumeration of colors of tomatos
    public enum TomatoColor
    {
        red, darkred, yellow, green
    }
}
