﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chief.Enums
{
    // Enumeration of conditions of fennel
    public enum FennelCondition
    {
        fresh, dry, seeds
    }
}
