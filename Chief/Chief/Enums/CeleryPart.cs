﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chief.Enums
{
    // Enumeration of parts of celery
    public enum CeleryPart
    {
        root, stem
    }
}
