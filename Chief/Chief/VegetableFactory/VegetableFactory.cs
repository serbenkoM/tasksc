﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chief.Vegetables;
using Chief.Enums;

namespace Chief.VegetableFactory
{
    public class VegetableFactory
    {
        /**
     * Creates specified vegetable with specified calorie and weight
     *
     * @param type         - type of vegetable to create
     * @param calories     - calorie per 100 gram
     * @param weight       - weight of vegetable
     * @param extraFiled   - type of the vegetable
     * @return specified Vegetable object
     */
        public static Vegetable getVegetable(string type, string country, double weight, double calories, string extraField)
        {
            Enums.Vegetables v = (Enums.Vegetables)Enum.Parse(typeof(Enums.Vegetables), type);
            switch (v)
                {
                    case Enums.Vegetables.Avocado:
                        return new Avocado(country, calories, weight);
                    case Enums.Vegetables.Cabbage:
                        Cabbage cabbage = new Cabbage(country, calories, weight);
                        cabbage.Sort = ((CabbageSort)Enum.Parse(typeof(CabbageSort), extraField));
                        return cabbage;
                    case Enums.Vegetables.Celery:
                        Celery celery = new Celery(country, calories, weight);
                        celery.Part = ((CeleryPart)Enum.Parse(typeof(CeleryPart), extraField));
                        return celery;
                    case Enums.Vegetables.Cucumber:
                        Cucumber cucumber =  new Cucumber(country, calories, weight);
                        cucumber.Type = ((CucumberType)Enum.Parse(typeof(CucumberType), extraField));
                        return cucumber;
                    case Enums.Vegetables.Fennel:
                        Fennel  fennel =  new Fennel(country, calories, weight);
                        fennel.Condition = ((FennelCondition)Enum.Parse(typeof(FennelCondition), extraField));
                        return fennel;
                    case Enums.Vegetables.Tomato:
                        Tomato tomato = new Tomato(country, calories, weight);
                        tomato.Color = ((TomatoColor)Enum.Parse(typeof(TomatoColor), extraField));
                        return tomato;
                    default:
                        throw new ArgumentException();
                }
        }

        /**
        * Creates specified vegetable with specified calorie and weight
        *
        * @param type     - type of vegetable to create
        * @param calories - calorie per 100 gram
        * @param weight   - weight of vegetable
        * @return specified Vegetable object
        */
        public static Vegetable getVegetable(string type, string country, double weight, double calories)
        {
            Enums.Vegetables v = (Enums.Vegetables)Enum.Parse(typeof(Enums.Vegetables), type);
            switch (v)
            {
                case Enums.Vegetables.Avocado:
                    return new Avocado(country, calories, weight);
                case Enums.Vegetables.Cabbage:
                    return new Cabbage(country, calories, weight);
                case Enums.Vegetables.Celery:
                    return new Celery(country, calories, weight);
                case Enums.Vegetables.Cucumber:
                    return new Cucumber(country, calories, weight);
                case Enums.Vegetables.Fennel:
                    return new Fennel(country, calories, weight);
                case Enums.Vegetables.Tomato:
                    return new Tomato(country, calories, weight);
                default:
                    throw new ArgumentException();
            }
        }
    }   
}
