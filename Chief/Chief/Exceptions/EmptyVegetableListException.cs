﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chief.Exceptions
{
    public class EmptyVegetableListException : Exception
    {
        public EmptyVegetableListException() { }

        public EmptyVegetableListException(string message)
            :base(message){ }

        public EmptyVegetableListException(string message, Exception inner)
        : base(message, inner)
        { }
    }
}
