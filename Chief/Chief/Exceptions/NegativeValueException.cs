﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chief.Exceptions
{
    public class NegativeValueException : Exception
    {
        public NegativeValueException() { }

        public NegativeValueException(string message)
            :base(message){ }

        public NegativeValueException(string message, Exception inner)
        : base(message, inner)
        { }
    }
}
