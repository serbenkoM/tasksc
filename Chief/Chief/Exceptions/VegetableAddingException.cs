﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chief.Exceptions
{
    public class VegetableAddingException : Exception
    {
        public VegetableAddingException() { }

        public VegetableAddingException(string message)
            :base(message){ }

        public VegetableAddingException(string message, Exception inner)
        : base(message, inner)
        { }
    }
}
