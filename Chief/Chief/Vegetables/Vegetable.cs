﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using Newtonsoft.Json;
using System.Xml.Serialization;

namespace Chief.Vegetables
{
    //An abstarct class of vegetables
    [Serializable()]
    [XmlType("Vegetable")]
    [XmlInclude( typeof(Avocado)), XmlInclude(typeof(Cabbage)), XmlInclude(typeof(Celery)), XmlInclude(typeof(Cucumber)), XmlInclude(typeof(Fennel)), XmlInclude(typeof(Tomato))]
    public abstract class Vegetable 
    {
        private string country;
        private double calories;
        private double weight;

        public Vegetable() { }
        /**
            * Constructor
            * @param country  specifies country of vegetables origin
            * @param calories specifies calorie per 100 gram of vegetables
            * @param weight   specifies weight of vegetables
        */
        public Vegetable(string country, double calories, double weight)
        {
            this.country = country;
            this.calories = calories;
            this.weight = weight;
        }

        //Gets total calories of vegetable in salad
        public double getTotalCalories()
        {
            return calories * usedWeight() / 100;

        }

        //Gets & sets weight of vegetable
        [XmlElement("weight")]
        public double Weight
        {
            get { return this.weight; }
            set { this.weight = value; }
        }

        //Gets & sets caloric value of vegetable per 100 gramms
        [XmlElement("calories")]
        public double Calories
        {
            get { return this.calories; }
            set { this.calories = value;  }
        }

        //Gets & sets origin country of vegetable
        [XmlElement("country")]
        public string Country
        {
            get { return this.country; }
            set { this.country = value; }
        }
        public override string ToString()
        {
            return "Calories: " + this.calories + "; weight: " + this.weight + "; country:" + this.country;
        }

        //Gets used weight of vegetable in salad
        public abstract double usedWeight();

        public abstract bool Equals(Vegetable obj);
    }
}
