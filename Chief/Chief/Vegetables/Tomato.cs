﻿using System;
using System.Xml.Serialization;
using Chief.Enums;
using Newtonsoft.Json;
using System.IO;
using System.Text;
namespace Chief.Vegetables
{
    [Serializable()]
    [XmlType("Tomato")]
    public class Tomato : Vegetable
    {
        
        private TomatoColor color;

        public Tomato() { }
        /**
          * Constructor
          * @param country   specifies country of tomato origin
          * @param calories  specifies calorie per 100 gram of tomato
          * @param weight    specifies weight of tomato
      */
        public Tomato(string country, double calories, double weight)
            : base(country, calories, weight) { }

        //Gets & sets  color of tomato
        [XmlElement("color")]
        public TomatoColor Color
        {
            get { return this.color; }
            set { this.color = value; }
        }

        //Gets total calories of tomato
        new public double getTotalCalories()
        {
            switch (this.color)
            {
                case TomatoColor.darkred:
                    return 1.2 * base.getTotalCalories();
                case TomatoColor.green:
                    return 0.87 * base.getTotalCalories();
                case TomatoColor.red:
                    return base.getTotalCalories();
                case TomatoColor.yellow:
                    return 1.1 * base.getTotalCalories();
                default:
                    return base.getTotalCalories();
            }
        }

        //Gets used weight of tomato in salad
        public override double usedWeight()
        {
            return 0.98 * Weight;
        }

        public override bool Equals(Vegetable obj)
        {
            Tomato tomato = (Tomato)obj;
            if (this.color == tomato.color)
                return true;
            else
                return false;
        }

        public override string ToString()
        {
            return base.ToString() + "; color: " + this.color;
        }
        
    }
}
