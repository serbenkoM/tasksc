﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Chief.Vegetables
{
    [Serializable()]
    [XmlType("Avocado")]
    public class Avocado : Vegetable
    {
        public Avocado() { }
        /**
            * Constructor
            * @param country  specifies country of avocado origin
            * @param calories specifies calorie per 100 gram of avocado
            * @param weight   specifies weight of avocado
        */
        public Avocado(string country, double calories, double weight)
            : base(country, calories, weight) { }

        public override bool Equals(Vegetable obj)
        {
            return true;
        }

        //Gets used weight of avocado in salad
        public override double usedWeight()
        {
            return 0.85 * base.Weight;
        }
    }
}
