﻿using System;
using System.Xml.Serialization;
using Chief.Enums;
using Newtonsoft.Json;

namespace Chief.Vegetables
{
    [Serializable()]
    [XmlType("Celery")]
    public class Celery : Vegetable
    {
        private CeleryPart part;

        public Celery() { }
        /**
            * Constructor
            * @param country  specifies country of celery origin
            * @param calories specifies calorie per 100 gram of celery
            * @param weight   specifies weight of celery
        */
        public Celery(string country, double calories, double weight)
            : base(country, calories, weight) { }

        //Set part of celery, which is used in salad
        [XmlElement("part")]
        public CeleryPart Part
        {
            get { return this.part; }
            set { this.part = value; }
        }

        //Gets total calories of celery
        new public double getTotalCalories()
        {
            switch (this.part)
            {
                case CeleryPart.root:
                    return 2.6 * base.getTotalCalories();
                case CeleryPart.stem:
                    return base.getTotalCalories();
                default:
                    return base.getTotalCalories();
            }
        }

        //Gets used weight of celery in salad
        public override double usedWeight()
        {
            switch (this.part)
            {
                case CeleryPart.root:
                    return base.Weight;
                case CeleryPart.stem:
                    return 0.95 * base.Weight;
                default:
                    return base.Weight;
            }
        }

        public override bool Equals(Vegetable obj)
        {
            Celery celery = (Celery)obj;
            if (this.part == celery.part)
                return true;
            else
                return false;
        }

        public override string ToString()
        {
            return base.ToString() + "; part: " + this.part;
        }


    }
}
