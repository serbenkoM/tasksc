﻿using System;
using System.Xml.Serialization;
using Chief.Enums;
using Newtonsoft.Json;

namespace Chief.Vegetables
{
    [Serializable()]
    [XmlType("Fennel")]
    public class Fennel : Vegetable
    {
        private FennelCondition condition;

        public Fennel() { }
        /**
           * Constructor
           * @param country   specifies country of fennel origin
           * @param calories  specifies calorie per 100 gram of fennel
           * @param weight    specifies weight of fennel
       */
        public Fennel(string country, double calories, double weight)
            : base (country, calories, weight) { }

        //Gets & sets  condition of fennel
        [XmlElement("condition")]
        public FennelCondition Condition
        {
            get { return this.condition; }
            set { this.condition = value; }
        }

        //Gets total calories of fennel
        new public double getTotalCalories()
        {
            switch (this.condition)
            {
                case FennelCondition.fresh:
                    return base.getTotalCalories();
                case FennelCondition.dry:
                    return 6.5 * base.getTotalCalories();
                case FennelCondition.seeds:
                    return 8.02 * base.getTotalCalories();
                default:
                    return base.getTotalCalories();
            }
        }

        //Gets  used weight of fennel in salad
        public override double usedWeight()
        {
            return base.Weight;
        }

        public override bool Equals(Vegetable obj)
        {
            Fennel fennel = (Fennel)obj;
            if (this.condition == fennel.condition)
                return true;
            else
                return false;
        }

        public override string ToString()
        {
            return base.ToString() + "; condition: " + this.condition;
        }
    }
}
