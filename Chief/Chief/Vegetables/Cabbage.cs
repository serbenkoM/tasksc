﻿using System;
using System.Xml.Serialization;
using Chief.Enums;
using Newtonsoft.Json;

namespace Chief.Vegetables
{
    [Serializable()]
    [XmlType("Cabbage")]
    public class Cabbage : Vegetable
    {
        private CabbageSort sort;

        public Cabbage() { }
        /**
            * Constructor
            * @param country  specifies country of cabbage origin
            * @param calories specifies calorie per 100 gram of cabbage
            * @param weight   specifies weight of cabbage
        */
        public Cabbage(string country, double calories, double weight)
            : base(country, calories, weight) { }

        //Set sort of cabbage
        [XmlElement("sort")]
        public CabbageSort Sort
        {
            get { return this.Sort; }
            set { this.sort = value; }
        }

        //Gets total calories of cabbage
        new public double getTotalCalories()
        {
            switch (this.sort)
            {
                case CabbageSort.white:
                    return base.getTotalCalories();
                case CabbageSort.cauliflower:
                    return 1.1 * base.getTotalCalories();
                case CabbageSort.beijing:
                    return 0.6 * base.getTotalCalories();
                case CabbageSort.broccoli:
                    return 0.89 * base.getTotalCalories();
                case CabbageSort.brussels:
                    return 1.3 * base.getTotalCalories();
                case CabbageSort.kohlrabi:
                    return 1.5 * base.getTotalCalories();
                default:
                    throw new ArgumentException();
            }
        }

        //Gets used weight of cabbage in salad
        public override double usedWeight()
        {
            switch (this.sort)
            {
                case CabbageSort.white:
                    return 0.95 * base.Weight;
                case CabbageSort.cauliflower:
                    return 0.95 * base.Weight;
                case CabbageSort.beijing:
                    return 0.85 * base.Weight;
                case CabbageSort.broccoli:
                    return base.Weight;
                case CabbageSort.brussels:
                    return base.Weight;
                case CabbageSort.kohlrabi:
                    return 0.95 * base.Weight;
                default:
                    throw new ArgumentException();
            }
        }

        public override bool Equals(Vegetable obj)
        {
            Cabbage cabbage = (Cabbage) obj;
            if (this.sort == cabbage.sort)
                return true;
            else
                return false;
        }

        public override string ToString()
        {
            return base.ToString() + "; sort: " + this.sort;
        }
    }
}
