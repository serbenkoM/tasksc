﻿using System;
using System.Xml.Serialization;
using Chief.Enums;
using Newtonsoft.Json;
namespace Chief.Vegetables
{
    [Serializable()]
    [XmlType("Cucumber")]
    public class Cucumber : Vegetable
    {
        
        private CucumberType type;

        public Cucumber() { }
        /**
           * Constructor
           * @param country  specifies country of cucumber origin
           * @param calories specifies calorie per 100 gram of cucumber
           * @param weight   specifies weight of cucumber
       */
        public Cucumber(string country, double calories, double weight)
            : base(country, calories, weight) { }

        //Gets & sets type of cucumber
        [XmlElement("type")]
        public CucumberType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        //Gets total calories of cucumber
        new public double getTotalCalories()
        {
            switch (this.type)
            {
                case CucumberType.pickled:
                    return 1.4 * base.getTotalCalories();
                case CucumberType.fresh:
                    return base.getTotalCalories();
                default:
                    return base.getTotalCalories();
            }
        }

        //Gets used weight of cucumber in salad
        public override double usedWeight()
        {
            switch (this.type)
            {
                case CucumberType.pickled:
                    return base.Weight;
                case CucumberType.fresh:
                    return 0.95 * base.Weight;
                default:
                    return base.Weight;
            }
        }

        public override bool Equals(Vegetable obj)
        {
            Cucumber cucumber = (Cucumber)obj;
            if (this.type == cucumber.type)
                return true;
            else
                return false;
        }

        public override string ToString()
        {
            return base.ToString() + "; type: " + this.type;
        }
    }
}
