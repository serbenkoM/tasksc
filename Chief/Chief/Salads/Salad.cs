﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Chief.Vegetables;
using Chief.Exceptions;

namespace Chief.Salads
{
    [Serializable()]
    [XmlType("Salad")]
    public class Salad
    {
        private List<Vegetable> vegetables = new List<Vegetable>();

        /**
            * Constructor
            * @param vegetables specifies list of vegetables in salad
        */
        public Salad(List<Vegetable> vegetables)
        {
            if (vegetables.Count != 0)
                this.vegetables.AddRange(vegetables);
            else
                throw new EmptyVegetableListException("Error! Couldn't create salad with no vegetables.");
        }

        public Salad() { }

        //Counts calories in salad
        public double getSaladCalories()
        {
            double calories = 0;
            for (int i = 0; i < this.vegetables.Count; i++)
            {
                calories += this.vegetables[i].getTotalCalories();
            }
            return calories;
        }

        //Gets & sets list of vegetables in salad
        [XmlArray("salad"), XmlArrayItem(typeof(Vegetable), ElementName = "Vegetable")]
        //[XmlElement("vegetables")]
        public List<Vegetable> Vegetables
        {
            get { return vegetables; }
            set { vegetables = value; }
        }

        //Counts weight of salad
        public double getSaladWeight()
        {
            double weight = 0;
            for(int i = 0; i < this.vegetables.Count; i++)
            {
                weight += this.vegetables[i].usedWeight();
            }
            return weight;
        }

        //Searches vegetables of salad with caloric value betwen min & max
        public List<Vegetable> searchVegetables(double minCalories, double maxCalories)
        {
            List<Vegetable> vegetablesList = new List<Vegetable>();
            for (int i = 0; i < this.vegetables.Count; i++)
            {
                if (this.vegetables[i].getTotalCalories() >= minCalories && this.vegetables[i].getTotalCalories() <= maxCalories)
                    vegetablesList.Add(this.vegetables[i]);
            }
            return vegetablesList;
        }

        //Sorts vegetables of salad by calories
        public IEnumerable<Vegetable> sortVegetablesByCalories()
        {
            IEnumerable<Vegetable> vegetablesList = new List<Vegetable>();
            vegetablesList = this.vegetables;
            return vegetablesList.OrderBy(Vegetable => Vegetable.getTotalCalories());
        }

        //Sorts vegetables of salad by weight
        public IEnumerable<Vegetable> sortVegetablesByWeight()
        {
            IEnumerable<Vegetable> vegetablesList = new List<Vegetable>();
            vegetablesList = this.vegetables;
            return vegetablesList.OrderBy(Vegetable => Vegetable.usedWeight());
        }

        //Adds vegetable to salad
        public void addVegetable(Vegetable vegetable)
        {
            if (!isVegetableInSalad(vegetable))
                vegetables.Add(vegetable);
            else
                throw new VegetableAddingException(vegetable.GetType().Name + " is already in salad.");
        }

        public bool isVegetableInSalad(Vegetable vegetable)
        {
            foreach (Vegetable v in this.vegetables)
            {
                if(v.GetType().Equals(vegetable.GetType()))
                    if (v.Equals(vegetable))
                    return true;
            }                
            return false;
        }
    }
}
