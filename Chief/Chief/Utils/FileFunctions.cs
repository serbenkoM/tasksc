﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chief.Salads;
using Chief.Vegetables;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Chief.Utils
{
    public class FileFunctions
    {
        private const string WEIGHT = "weight";
        private const string CALORIES = "calories";
        private const string COUNTRY = "country";
        private const string TXT_FILE_NAME = @"salads.txt";
        private const string BINARY_FILE_NAME = @"salads.bin";
        private const string XML_FILE_NAME = @"salads.xml";

        public static void AddSaladInTxtFile(Salad salad)
        {
            using (System.IO.StreamWriter file =
             new System.IO.StreamWriter(TXT_FILE_NAME, true))
            {
                file.WriteLine("Salad");
                foreach (Vegetables.Vegetable v in salad.Vegetables)
                {
                    file.WriteLine(v.GetType().Name + "; " + v.ToString());
                }
            }
        }

        public static List<Salad> ReadTxtFile()
        {
            string extraField = null, vegetableType, country = null;
            double weight = 0, calories = 0;
            string[] separator = new string[] { "Salad" };
            string[] salads;
            List<Salad> saladsList = new List<Salad>();
            System.IO.StreamReader file =
                new System.IO.StreamReader(TXT_FILE_NAME);
            salads = file.ReadToEnd().Split(separator, StringSplitOptions.RemoveEmptyEntries);
            file.Close();
            for (int i = 0; i < salads.Length; i++)
            {
                List<Vegetable> vegetablesList = new List<Vegetable>();
                foreach (string st in salads[i].Split('\n').Where(n => !string.IsNullOrWhiteSpace(n)).ToArray())
                {
                    vegetableType = st.Substring(0, st.IndexOf(" ") - 1);
                    string[] parametrs = st.Substring(st.IndexOf(" ")).Split(';');
                    foreach (string param in parametrs)
                    {
                        string paramName = param.Split(':')[0];
                        string paramValue = param.Split(':')[1];
                        if (paramName.ToLower().Contains(WEIGHT))
                            weight = Convert.ToDouble(paramValue);
                        else
                            if (paramName.ToLower().Contains(CALORIES))
                            calories = Convert.ToDouble(paramValue);
                        else
                                if (paramName.ToLower().Contains(COUNTRY))
                            country = paramValue;
                        else
                            extraField = paramValue;
                    }
                    vegetablesList.Add(VegetableFactory.VegetableFactory.getVegetable(vegetableType, country, weight, calories, extraField));
                }
                saladsList.Add(new Salad(vegetablesList));
            }
            return saladsList;
        }

        public static void AddSaladInBinaryFile(Salad salad)
        {
            Stream TestFileStream = File.Open(BINARY_FILE_NAME, FileMode.Append);
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(TestFileStream, salad);
            TestFileStream.Close();
        }

        public static List<Salad> ReadBinaryFile()
        {
            Stream TestFileStream = File.OpenRead(BINARY_FILE_NAME);
            BinaryFormatter deserializer = new BinaryFormatter();
            List<Salad> saladList = new List<Salad>();
            while (TestFileStream.Length != TestFileStream.Position)
                saladList.Add((Salad)deserializer.Deserialize(TestFileStream));
            TestFileStream.Close();
            return saladList;
        }

        public static void SerializeToXML(Salad salad)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Salad));
            FileStream fs = new FileStream(XML_FILE_NAME, FileMode.OpenOrCreate);
            serializer.Serialize(fs, salad);
            fs.Close();
        }

        public static Salad DeserializeFromXML()
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(Salad));
            FileStream fs = new FileStream(XML_FILE_NAME, FileMode.Open);
            Salad salads;
            salads = (Salad)deserializer.Deserialize(fs);
            fs.Close();
            return salads;
        }
    }
}
